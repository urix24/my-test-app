Next.js app in Docker environment.

Usage:

Write your code, next make git commit.

Git commit  hook would stop current app in docker container, build new docker image 
and start it.

App available at current hosts  3000 tcp port.

git commit hook makes following:
docker stop next_app
docker rm next_app
docker build -t node:test .
docker-compose up -d
 
