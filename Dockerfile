FROM node:alpine as builder

WORKDIR /app
COPY . .
RUN  npm install && npm run build 

FROM node:alpine
WORKDIR /app

COPY --from=builder /app/package.json ./package.json 
COPY --from=builder /app/node_modules ./node_modules 
COPY --from=builder /app/.next ./.next 
COPY --from=builder /app/public ./public 
COPY --from=builder /app/pages ./pages

ENTRYPOINT ["npm", "start"]







